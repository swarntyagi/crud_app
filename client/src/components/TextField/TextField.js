import React from "react";
import cn from "classnames";
import PropTypes from "prop-types";

import "./TextField.scss";

const TextField = ({
  className,
  type,
  name,
  value,
  onChange: cb,
  placeholder,
}) => {
  const onHandleChange = ({ target: { value } }) => {
    cb && cb(name, value);
  };
  return (
    <div className={cn("TextField", className)}>
      <input
        type={type}
        name={name}
        value={value}
        onChange={onHandleChange}
        placeholder={placeholder}
        className={cn("TextField-Input")}
      />
    </div>
  );
};

TextField.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  className: PropTypes.string,
  placeholder: PropTypes.string,
};

export default TextField;
