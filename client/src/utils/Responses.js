export class SuccessResponse {
  constructor(response) {
    this.data = response.body;
    this.status = response.body.statusCode;
  }
}
export class ErrorResponse {
  constructor({ response: { body: { message } = {} } = {}, status }) {
    this.message = message;
    this.status = status;
  }
}
