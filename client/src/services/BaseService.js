import request from "superagent";

import config from "../config";
import { SuccessResponse, ErrorResponse } from "../utils/Responses";

function getRoute(route) {
  return route.includes("/") ? route : route + "/";
}
class BaseService {
  constructor(route) {
    this.route = getRoute(route);
    //this.save = this.save.bind(this);
    this.find = this.find.bind(this);
    // this.update = this.update.bind(this);
    // this.delete = this.delete.bind(this);
    this.request = this.request.bind(this);
    //  this.findById = this.findById.bind(this);
  }
  async find(params) {
    return this.request({ params, url: this.route });
  }

  async request(opts = {}) {
    opts = Object.assign({ method: "GET", url: "" }, opts);

    let { method, params, url } = opts;

    const { baseURL } = config;

    url = `${baseURL}${url}`;

    switch (method) {
      case "GET": {
        try {
          const response = await request.get(url).query(params);
          console.log("response", response);
          return new SuccessResponse(response);
        } catch (error) {
          console.log("error");
          return new ErrorResponse(error);
        }
      }
      default: {
        try {
          const response = await request.get(url).query(params);
          return new SuccessResponse(response);
        } catch (error) {
          return new ErrorResponse(error);
        }
      }
    }
  }
}

export default BaseService;
