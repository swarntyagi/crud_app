import BaseService from "./BaseService";

class Service extends BaseService {
  constructor() {
    super("users");
  }
}

export default Service;
