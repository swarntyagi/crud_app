import React, { useEffect } from "react";

import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import LoginPage from "./containers/LoginPage/LoginPage";

import Service from "./services/UserService";

const service = new Service();
export default function App(props) {
  useEffect(() => {
    service.find().then((data) => {
      console.log("Data", data);
    });
  }, []);
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path={"/login"} component={LoginPage} />
        <Redirect from="/" to="/login" />
      </Switch>
    </BrowserRouter>
  );
}
