import React from "react";

import Form from "./../../components/Form/Form";
import TextField from "./../../components/TextField/TextField";

import "./LoginPage.scss";

const Login = () => {
  const onChange = () => {};
  return (
    <div className="LoginPageWrapper">
      <section className="LoginPage">
        <header className="Header">
          <span className="HeaderText">Dukaan</span>
          <span className="Login">Login</span>
          <span className="Signup">Signup</span>
        </header>
        <Form>
          <TextField
            name="userName"
            onChange={onChange}
            placeholder="Email Address"
          />
          <TextField
            type="password"
            name="password"
            onChange={onChange}
            placeholder="Email Address"
          />
        </Form>
      </section>
    </div>
  );
};

export default Login;
