class ErrorResponse extends Error {
  constructor(err) {
    super();
    this.success = false;
    this.status = 400;
    this.message = err.message;
  }
}

class ListResponse {
  constructor(data) {
    this.success = true;
    this.results = data;
    this.totalCount = data.length;
  }
}

class ObjectResponse {
  constructor(data) {
    this.success = true;
    this.results = data.length ? data[0] : null;
  }
}

class SaveOrUpdateResponse {
  constructor(id) {
    this.success = true;
    this.results = { id };
  }
}

class DeleteResponse {
  constructor() {
    this.success = true;
    this.message = "Successfully Deleted";
  }
}

module.exports = {
  ListResponse,
  ErrorResponse,
  DeleteResponse,
  ObjectResponse,
  SaveOrUpdateResponse,
};
