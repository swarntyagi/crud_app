function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
}

function isArray(array) {
  return Array.isArray(array);
}

module.exports = {
  isArray,
  isEmpty,
};
