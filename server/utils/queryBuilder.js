const { isEmpty, isArray } = require("./utilityFunctions");

class QueryBuilder {
  constructor({ columns = "", tableName = "" }) {
    this.allColumns = columns;
    this.tableName = tableName;
  }

  select(options = {}) {
    const { columns = "*", where } = options;

    return `select ${getColumns(columns, this.allColumns)} from ${
      this.tableName
    } ${getWhereClause(where)}`;
  }

  //TODO: make insert query builder
  insert(options = {}) {
    const { columns = "*", where } = options;

    return `select ${getColumns(columns, this.allColumns)} from ${
      this.tableName
    } ${getWhereClause(where)}`;
  }

  //TODO: make update query builder
  update(options = {}) {
    const { columns = "*", where } = options;

    return `select ${getColumns(columns, this.allColumns)} from ${
      this.tableName
    } ${getWhereClause(where)}`;
  }

  //TODO: make delete query builder
  delete(options = {}) {
    const { columns = "*", where } = options;

    return `select ${getColumns(columns, this.allColumns)} from ${
      this.tableName
    } ${getWhereClause(where)}`;
  }
}

function getColumns(columns, allColumns) {
  if (isArray(columns)) {
    return columns.length ? columns : "*";
    //} else if (columns.areExcludedColumns) {
  } else {
    return columns.trim().length ? columns : "*";
  }
}

function getWhereClause(whereConditions) {
  let whereClause = "";

  if (!isEmpty(whereConditions)) {
    whereClause = "where ";

    let columns = Object.keys(whereConditions);

    columns = columns.map((column) => {
      const { value, condition = "=" } = whereConditions[column];
      return `${column} ${condition} '${value}'`;
    });

    whereClause += columns.join(" and ");
  }

  return whereClause;
}

module.exports = QueryBuilder;
