const sql = require("mysql2");

const {
  ListResponse,
  ErrorResponse,
  DeleteResponse,
  ObjectResponse,
  SaveOrUpdateResponse,
} = require("./serverResponse");

con = sql.createConnection({
  user: "root",
  password: "root",
  host: "localhost",
  database: "swarn_learning",
});

con.connect(function (err) {
  if (err) throw err;
  console.log("DB Connected!");
});

const promiseQuery = con.promise();

/**
 * Old Way For Query, when not using query builder
 * If want to expose direct query for Frontend
 * Then we can use it
 */
const useQuery = (query, fields) => {
  return fields ? promiseQuery.query(query, fields) : promiseQuery.query(query);
};

const find = async (query, fields) => {
  try {
    const [data] = await useQuery(query, fields);

    return new ListResponse(data);
  } catch (err) {
    console.log("Throwing Error", err);
    throw new ErrorResponse(err);
  }
};

const findById = async (query, fields) => {
  try {
    const [data] = await useQuery(query, fields);

    return new ObjectResponse(data);
  } catch (err) {
    return new ErrorResponse(err);
  }
};

const save = async (query, fields) => {
  try {
    const [data] = await useQuery(query, fields),
      { insertId } = data;

    return new SaveOrUpdateResponse(insertId);
  } catch (err) {
    return new ErrorResponse(err);
  }
};

const update = async (query, fields, id) => {
  try {
    const [data] = await useQuery(query, fields),
      { affectedRows } = data;

    return affectedRows
      ? new SaveOrUpdateResponse(id)
      : new ErrorResponse({ message: "Id Doesn't Exist" });
  } catch (err) {
    return new ErrorResponse(err);
  }
};

const deleteById = async (query, fields) => {
  try {
    const [data] = await useQuery(query, fields),
      { affectedRows } = data;

    return affectedRows
      ? new DeleteResponse()
      : new ErrorResponse({ message: "Id Doesn't Exist" });
  } catch (err) {
    return new ErrorResponse(err);
  }
};

module.exports = {
  save,
  find,
  update,
  findById,
  deleteById,
};
