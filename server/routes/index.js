const cors = require("cors");
const bodyParser = require("body-parser");

const express = require("express");

const server = express();
server.use(cors());

server.use(bodyParser.json({ extended: true }));

const user = require("./user");

server.use("/users", user);

server.use((error, req, res, next) => {
  res.status(400);
  res.json(error);
});
module.exports = server;
