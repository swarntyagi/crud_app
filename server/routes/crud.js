const ash = require("express-async-handler");
const {
  save,
  find,
  update,
  findById,
  deleteById,
} = require("../utils/dbOperations");

function getCRUDRoutes({ tableName }) {
  const crud = require("express").Router(),
    QueryBuilder = require("./../utils/queryBuilder"),
    qb = new QueryBuilder({ tableName });

  crud
    .get(
      "/",
      ash(async (req, res) => {
        try {
          res.send(await find(qb.select()));
        } catch (error) {
          console.log("err in crud", error);
          //next(error);
          //return res.status(401).json(error);
          throw error;
        }
      })
    )
    .get("/:id", async (req, res) => {
      const { id } = req.params;
      res.send(
        await findById(
          qb.select({
            where: { id: { value: id } },
          })
        )
      );
    })
    .post("/new", async (req, res) => {
      const { name, age, qualification } = req.body;
      res.send(
        await save(
          `insert into ${TABLE_NAME} (name, age, qualification) values(?, ?, ?)`,
          [name, age, qualification]
        )
      );
    })
    .put("/:id", async (req, res) => {
      const {
        body: { id, name, age, qualification },
      } = req;

      res.send(
        await update(
          `update ${TABLE_NAME} set name = ? ,age = ?, qualification = ? where id = ?`,
          [name, age, qualification, id],
          id
        )
      );
    })
    .delete("/:id", async (req, res) => {
      const { id } = req.params;

      res.send(
        await deleteById(`delete from ${TABLE_NAME} where id = ?`, [id])
      );
    });

  return crud;
}

module.exports = getCRUDRoutes;
